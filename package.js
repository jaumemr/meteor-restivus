Package.describe({
  name: 'jaume:restivus',
  summary: 'Create authenticated REST APIs in Meteor 1.6+ via HTTP/HTTPS. Setup CRUD endpoints for Collections.',
  version: '0.8.16',
  git: 'https://bitbucket.org/jaumemr/meteor-restivus'
});


Package.onUse(function (api) {
  // Minimum Meteor version
  api.versionsFrom('METEOR@1.5.2');

  // Meteor dependencies
  api.use('check');
  api.use('coffeescript@2.0.3_1');
  api.use('underscore');
  api.use('accounts-password');
  api.use('simple:json-routes@2.1.0');

  api.addFiles('lib/auth.coffee', 'server');
  api.addFiles('lib/iron-router-error-to-response.js', 'server');
  api.addFiles('lib/route.coffee', 'server');
  api.addFiles('lib/restivus.coffee', 'server');

  // Exports
  api.export('Restivus', 'server');
});

